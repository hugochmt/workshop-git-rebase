---
marp: true
# theme: gaia
backgroundColor: #FFEFC0
style: |
  section {
    justify-content: start;
  }
---


# Atelier Dev
## Git - Rebase

![bg left:40% 80%](https://iconape.com/wp-content/png_logo_vector/git-icon.png)


---
## Merge vs Rebase
![bg w:800](./img/init.svg)


---
## Merge
- Merge de main dans feature
- `git merge main`
- Un commit de merge est alors créé
- Non-destructif : branches non altérées
![bg right w:700](./img/merge.svg)


---
## Rebase
- Rebase de feature sur main
- `git rebase main`
- Réécriture de l'historique pour chaque commit : `git push -f`
- Historique propre

![bg right:50% w:650](./img/rebase.svg)


---
## Rebase
- 🚫 Ne jamais rebase sur une branche publique !
- 🆗 Merge ou rebase, le résultat final est le même, seul l'historique change


---
## Rebase interactif
- Possibilité de modifier les commits déplacés
  - Depuis une branche : `git rebase -i main`
  - Depuis un commit : `git rebase -i 33d5b7a`
  - Les `n` derniers commits : `git rebase -i HEAD~n`
- L'éditeur affiche les commits qui seront déplacés
```
pick 33d5b7a Message for commit #1
pick 9480b3d Message for commit #2
pick 5c67e61 Message for commit #3
```

---
## Rebase interactif
- Parcours de chacun des commits
- Action possible sur chacun des commits (modification, suppression, squash...)
- Continue jusqu'à la prochaine action ou la prochaine erreur (conflits...) :
  - `git rebase --continue`


---
## Rebase interactif
- Commandes rebase interfactif *#RTFM* :
  - 🍒 `pick` : Utiliser le commit sans modification 
  - 🛠 `edit` : Utiliser le commit et s'arrêter dessus pour le modifier
  - ✏ `reword` : Modifier le message du commit
  - 📚 `squash` : Joindre plusieurs commits en un seul
  - 🗑 `drop` : Enlève le commit
- Commandes utiles :
  - 😌 : `git rebase --continue`
  - 🤨 : `git status` , `git log --graph --oneline`
  - 🤯 : `git rebase --abort`

---
## Exemple
Ajout d'un commit **D** après le commit **C** :
``` console
git rebase -i c81cb38 //Rebase sur le commit b, pour pouvoir modifier le commit C

/*  edit 72b172c C */
/*  update file    */

git add file.txt
git commit -m "D"
git rebase --continue
```


---
## Rappel : git commit amend
- Modification du dernier commit : `git commit --amend`
- Combinaison des changements avec l'ancien commit plutôt que créer un nouveau commit
- Modification du message : `git commit --amend -m "an updated commit message"`
- Ajout d'un fichier dans le dernier commit :
`git add file.js`
`git commit --amend --no-edit`



---
### Exercice 1
Ajouter un commit **Init** avant le commit A

### Exercice 2
En un seul `git rebase` :
- Modifier le contenu du commit et renommer **b** en **B**
- Ajouter un commit **E** après le commit **C**
- Supprimer commit **W**

### Exercice 3
Rebase sur la branche *main* et squash les commits en un seul "Un bon rebase"

---
# Sources

- Articles Atlassian :
  - [Merge vs Rebase](https://www.atlassian.com/fr/git/tutorials/merging-vs-rebasing)
  - [Rebase](https://www.atlassian.com/fr/git/tutorials/rewriting-history/git-rebase)

- [Documentation git](https://git-scm.com/docs/git-rebase)

Slides réalisées avec [Marp](https://marp.app/)
